/**
 * Author: Boyan M. Atanasov
 * Student No: 2322608
 * License: MIT
 */

#include <iostream>
/* unistd.h contains fork() */
#include <unistd.h>
#include <syslog.h>

/* umask */
#include <sys/types.h>
#include <sys/stat.h>

/* inode notify (inotify(2)) */
#include <sys/inotify.h>

int main() {

    pid_t pid, sid;
    /* fork off of the parent process */
    pid = fork();
    /* on bad PID we report an error and exit with a failure. */
    if(pid < 0) {
        printf("Child PID error. Spawning aborted.");
        exit(EXIT_FAILURE);
    }
    /* if we don't get 0 means we were the parent process
     * and we can successfully exit */
    if (pid > 0){
        printf("Child PID ok. Child daemon spawned successfully.");
        exit(EXIT_SUCCESS);
    }

    /* from THIS point onwards, daemon code
     *
     */
     /* set the umask permissions downgrade to 0.
     * That equals to no downgraded permissions or full permissions */
    umask(0);

    /* prepare for logging */
    openlog("fsctx", LOG_CONS | LOG_PID | LOG_NDELAY, LOG_LOCAL1);
    syslog(LOG_NOTICE, "Daemon started.");

    /* Open a unique session ID from kernel */
    sid = setsid();
    if(sid < 0) {
        syslog(LOG_ERR, "Daemon received bad SID. Daemon aborted.");
        exit(EXIT_FAILURE);
    } else {
        syslog(LOG_NOTICE, "Daemon received SID: %d", (int) sid);
    }

    /* Change the current working directory */
    if ((chdir("/")) < 0) {
        syslog(LOG_ERR, "Daemon couldn't change working directory.");
        exit(EXIT_FAILURE);
    } else {
        syslog(LOG_INFO, "Daemon changed working directory to \"/\"");

    }


    /* exit sequence */
    syslog(LOG_NOTICE, "Daemon finished execution. Now exiting...");
    exit(EXIT_SUCCESS);
}