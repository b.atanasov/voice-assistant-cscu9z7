cmake_minimum_required(VERSION 3.8)
project(fswatch_proc_project)

set(CMAKE_C_STANDARD 99)

set(SOURCE_FILES main.c)
add_executable(fswatch_proc_project ${SOURCE_FILES})